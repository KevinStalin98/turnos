import { Module } from '@nestjs/common';
import { CitaModule } from './cita/cita.module';
import { DatabaseModule } from './database/database.module';

@Module({
  imports: [
    CitaModule,
    DatabaseModule
  ],
})
export class ModulesModule {}
