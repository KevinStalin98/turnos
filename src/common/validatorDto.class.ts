import {
  ArgumentMetadata,
  HttpStatus,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { ValidationError, validate } from 'class-validator';
import { HttpException } from '@nestjs/common/exceptions/http.exception';
@Injectable()
export class CustomValidationPipeDto implements PipeTransform {
  async transform(value: any, metadata: ArgumentMetadata) {
    const { metatype } = metadata;
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }
    const object = plainToClass(metatype, value);
    const errors = await validate(object);
    if (errors.length > 0) {
      const errorMessages = this.formatErrors2(errors);
      throw new HttpException(
        {
          ok: false,
          codRetorno: '9999',
          message: 'Validators Pipe',
          retorno: errorMessages,
        },
        203, //HttpStatus.BAD_REQUEST,
      );
    }
    return value;
  }
  private formatErrors(errors: ValidationError[]) {
    return errors.reduce((formattedErrors, error) => {
      const constraints = error.constraints;
      if (constraints) {
        const field = error.property;
        const messages = Object.values(constraints);
        messages.forEach((message) => {
          formattedErrors.push({ field, message });
        });
      }
      if (error.children && error.children.length > 0) {
        formattedErrors.push(...this.formatErrors(error.children));
      }
      return formattedErrors;
    }, []);
  }
  private formatErrors2(errors: ValidationError[]) {
    return errors.reduce((errorMessages, error) => {
      const propertyPath = error.property;
      const messages = Object.values(error.constraints || {});
      if (error.children && error.children.length > 0) {
        messages.push(...this.formatErrors2(error.children));
      }
      const formattedErrors = messages.map(
        (message) => `${propertyPath} ${message}`,
      );
      return [...errorMessages, ...formattedErrors];
    }, []);
  }
​
  private toValidate(metatype: Function): boolean {
    const types = [String, Boolean, Number, Array, Object];
    return !types.find((type) => metatype === type);
  }
}