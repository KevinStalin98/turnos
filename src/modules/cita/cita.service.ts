import { Inject, Injectable } from '@nestjs/common';
import { CreateCitaDto } from './dto/create-cita.dto';
import { UpdateCitaDto } from './dto/update-cita.dto';
import { Repository } from 'typeorm';
import { Cita } from './entities/cita.entity';

@Injectable()
export class CitaService {
  constructor(
    @Inject('CITA_REPOSITORY')
    private photoRepository: Repository<Cita>,
  ) {}

  create(createCitaDto: CreateCitaDto) {
    return 'This action adds a new cita';
  }

  findAll() {
    return `This action returns all cita`;
  }

  findOne(id: number) {
    return `This action returns a #${id} cita`;
  }

  update(id: number, updateCitaDto: UpdateCitaDto) {
    return `This action updates a #${id} cita`;
  }

  remove(id: number) {
    return `This action removes a #${id} cita`;
  }
}
