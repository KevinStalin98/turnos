import { DataSource } from 'typeorm';
import { Cita } from './entities/cita.entity';

export const citaProviders = [
  {
    provide: 'CITA_REPOSITORY',
    useFactory: (dataSource: DataSource) => dataSource.getRepository(Cita),
    inject: ['DATA_SOURCE'],
  },
];
