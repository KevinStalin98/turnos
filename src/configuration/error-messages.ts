export class ErrorMessage {
  //*BLOQUE CON ERRORES DEl CATCH
  public static readonly ERROR = {
    message:
      'Al momento nos encontramos actualizando el sistema, intente mas tarde -> BDD',
    codRetorno: '9999',
    status: 505,
  };

  public static readonly ERROR_ENTITY = {
    message:
      'Al momento nos encontramos actualizando el sistema, intente mas tarde -> ENTITY',
    codRetorno: '9999',
    status: 505,
  };

  public static readonly ERROR_SERVICE = {
    message:
      'Los campos estan incorrectos o el servicio se encuentra actualizando, intente mas tarde -> SERVICE',
    codRetorno: '9999',
    status: 505,
  };

  public static readonly ERROR_CONTROLLER = {
    message:
      'Al momento nos encontramos actualizando el sistema, intente mas tarde -> CONTROLLER',
    codRetorno: '9999',
    status: 505,
  };

  public static readonly ERROR_WS_MAIL = {
    message:
      'Al momento nos encontramos actualizando el sistema, intente mas tarde -> ws-MAIL',
    codRetorno: '9999',
    status: 505,
  };

  public static readonly ERROR_WSDL = {
    message: 'FALLÓ CONSULTA WS',
    codRetorno: '9999',
    status: 500,
  };

  public static readonly ERROR_DATA = {
    message: 'DATOS INCORRECTOS',
    codRetorno: '0010',
    status: 203,
  };

  //* SON ERRORES DEl CATCH
  public static readonly dataFound = {
    message: 'Ya se encuentra registrado',
    codRetorno: '0010',
    status: 203,
  };

  public static readonly dataNotFound = {
    message: 'La consulta no retornó datos',
    codRetorno: '0010',
    status: 203,
  };

  public static readonly dataNotSave = {
    message: 'No se pudieron guardar los datos',
    codRetorno: '0010',
    status: 203,
  };
  public static readonly ERROR_AS400 = {
    code: 500,
    message: 'INTENTE MAS TARDE',
    codeResponse: '9999',
  };
  public static readonly noRecords = {
    message: 'No existen registros actualmente.',
    codRetorno: '0010',
    status: 203,
  };
  static ERROR_WS: any;
  public static readonly ERROR_CATCH = {
    message: 'ERROR TECNICO',
    codRetorno: '9999',
    status: 505,
  };
}
/*
/throw de clases dao
      throw new HttpException(
        {
          message: ErrorMessage.ERROR_AS400.message,
          codRetorno: ErrorMessage.ERROR_AS400.codeResponse,
        },
        ErrorMessage.ERROR_AS400.code,
        {
          cause: new Error(error.stack),
          description: error.message,
        },
      );
/throw de servicios
      throw new HttpException(
        {
          message: error.response?.message || ErrorMessage.ERROR_CATCH.message,
          codRetorno:
            error.response?.codRetorno || ErrorMessage.ERROR_CATCH.codeResponse,
        },
        error.status || ErrorMessage.ERROR_CATCH.code,
        {
          cause: new Error(error.options?.cause || error.stack),
          description: error.options?.description || error.message,
        },
      );
/Respuesta controlador
      return errorResponse(
        req,
        res,
        error.status || ErrorMessage,
        error.response.codRetorno || ErrorMessage,
        error.response.message || error.message,
        {
          casuse: error.options?.cause.stack,
          description: error.options?.description,
        } || ' ',
      );


*/
