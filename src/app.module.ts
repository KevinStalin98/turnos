import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ModulesModule } from './modules/modules.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import configuration from './configuration/configuration';

require('dotenv').config();

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
      envFilePath: `./env/${process.env.NODE_ENV}.env`,
      isGlobal: true,
    }),
    ModulesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private readonly configService: ConfigService) {
    console.log(
      `===============================================================================`,
    );
    console.log('\tCONFIGURACION APP');
    console.log(
      `===============================================================================`,
    );
    console.log(
      'Descripcion: Aplicativo para el manejo de Paersonas del esquema ANT',
    );
    console.log(`AMBIENTE:\t${process.env.NODE_ENV}`);
    console.log(`PUERTO:\t${process.env.PORT}`);
    console.log(
      `===============================================================================`,
    );
  }
}
