import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Cita {
  @PrimaryGeneratedColumn()
  idCita: number;
  @Column({ length: 500 })
  center: string;
  @Column('text')
  status: string;
  @Column()
  mail: string;
  @Column()
  cellphone: string;
  @Column()
  date: string;
  @Column()
  hour: string;
  @Column()
  plate: string;
  @Column()
  identification: string;
  @Column('int')
  phonenumber: number;
  @Column()
  chasis: string;
  @Column()
  type: string;

  //   @Column('int')
  //   views: number;
}
