export class OkMessage {

public static readonly OK = {
    message: 'Éxito al ejecutar la consulta',
    codRetorno: '0001',
    status: 200,
  };
}