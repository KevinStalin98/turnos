import { registerAs } from '@nestjs/config';

export default registerAs('dao', () => ({
  // host: process.env.host,
  PORT: process.env.PORT,
}));
