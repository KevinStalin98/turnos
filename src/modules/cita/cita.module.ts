import { Module } from '@nestjs/common';
import { CitaService } from './cita.service';
import { CitaController } from './cita.controller';
import { citaProviders } from './cita.providers';
import { DatabaseModule } from '../database/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [CitaController],
  providers: [...citaProviders, CitaService],
})
export class CitaModule {}
